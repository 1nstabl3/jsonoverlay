# JSON Overlay

A simple C-written library for creating an JSON (overlay) representation of program variables. The library does not store any data except for representation while holding references to the real data. That way the structure only needs to be defined once and after that the parser generates the JSON string output with the current value of the data.

## Features
The library aims at memory restricted embedded systems and thus seeks to be as lightweight and unintruding as possible.
Therefore the library is designed soley as overlay and not as means for storage of values. It uses the existing variables as storage and holds their location with (JSON) structural data such as object name (`key`) and child elements.

### Currently supported features
The library currently supports the following featuers:
* Adding and removing of nodes to a document
* Creating nodes
  * INT32
  * STRING
  * FLOAT
  * ARRAY
  * OBJECT
* Moving objects to another location within the tree
* Hiding/Unhiding nodes from output

### Not yet supported features
Due to lack of time, the following features are not yet implemented:
* Parsing of JSON strings into a tree
* Adding/Showing STRING arrays
* Adding/Showing OBJECT arrays

## Using the library
The library consists of two files: `src/jsonoverlay.h` and `src/jsonoverlay.c` 
* Copy the files to your project or use this repository as submodule
* add `src/` to your include path
* add `src/jsonoverlay.c` to your compilatoion list

## Testing
The library comes with simple test programs, located in `test/`. Unit tests and integration tests are planned but not yet added. 

CMake is used as build system.

## Documentation
The code is documented using Doxygen and inline comments.