#ifndef JSON_OVERLAY_H
#define JSON_OVERLAY_H

#include "jsoncommon.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Adds a STRING node to the given parent element
 * @details [long description]
 * 
 * @param root [description]
 * @param parent [description]
 * @param key [description]
 * @param value [description]
 * @return [description]
 */
JSON_RETURN_T json_add_key_str(tree_node_t* root, tree_node_t* parent, char* key, char* value);

/**
 * @brief Adds an INT32 node to the given parent element
 * @details [long description]
 * 
 * @param root [description]
 * @param parent [description]
 * @param key [description]
 * @param value [description]
 * @return [description]
 */
JSON_RETURN_T json_add_key_i32(tree_node_t* root, tree_node_t* parent, char* key, int32_t* value);

/**
 * @brief Adds an FLOAT node to the given parent element
 * @details [long description]
 * 
 * @param root [description]
 * @param parent [description]
 * @param key [description]
 * @param value [description]
 * @return [description]
 */
JSON_RETURN_T json_add_key_f(tree_node_t* root, tree_node_t* parent, char* key, float* value);

/**
 * @brief Adds an OBJECT node to the given parent element
 * @details [long description]
 * 
 * @param root [description]
 * @param parent [description]
 * @param self [description]
 * @param key [description]
 * @return [description]
 */
JSON_RETURN_T json_add_obj(tree_node_t* root, tree_node_t* parent, tree_node_t** self, char* key);

/**
 * @brief Adds an ARRAY of INT32 to the given parent element
 * @details [long description]
 * 
 * @param root [description]
 * @param parent [description]
 * @param key [description]
 * @param val [description]
 * @param val_len [description]
 * @return [description]
 */
JSON_RETURN_T json_add_array_i32(tree_node_t* root, tree_node_t* parent, char* key, int32_t* val, uint32_t val_len);

/**
 * @brief Adds an ARRAY of FLOAT to the given parent element
 * @details [long description]
 * 
 * @param root [description]
 * @param parent [description]
 * @param key [description]
 * @param val [description]
 * @param val_len [description]
 * @return [description]
 */
JSON_RETURN_T json_add_array_f(tree_node_t* root, tree_node_t* parent, char* key, float* val, uint32_t val_len);

/**
 * @brief Adds a node to the given parent destination
 * @details [long description]
 * 
 * @param dest [description]
 * @param node [description]
 * 
 * @return [description]
 */
JSON_RETURN_T json_add_node(tree_node_t* dest, tree_node_t* node);

/**
 * @brief Deletes a given node and all its children
 * @details [long description]
 * 
 * @param node [description]
 * @return [description]
 */
JSON_RETURN_T json_remove_node(tree_node_t* node);

/**
 * @brief Moves a node and its children to a new location in the tree
 * @details [long description]
 * 
 * @param dest [description]
 * @param node [description]
 * 
 * @return [description]
 */
JSON_RETURN_T json_move_node(tree_node_t* dest, tree_node_t* node);

/**
 * @brief Generates a JSON string from a given document
 * @details [long description]
 * 
 * @param root [description]
 * @param str_buffer [description]
 * @param buffer_size [description]
 * @param pretty [description]
 * @return [description]
 */
JSON_RETURN_T json_build(tree_node_t* root, char* str_buffer, uint32_t buffer_size, bool pretty);

#ifdef __cplusplus
}
#endif


#endif