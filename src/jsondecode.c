#ifndef JSON_DECODE
#define JSON_DECODE

#include "jsondecode.h"

#include <string.h>
#include <assert.h>
#include <stdlib.h>
#include <stdio.h>


#ifdef __cplusplus
extern "C" {
#endif

typedef struct raw_field
{
	char* key_begin;
	uint_fast8_t key_len;
	char* field_begin;
	uint_fast8_t field_len;
	bool bool_val; 	// can already be evaluated while determining field type
	JSON_NODE_TYPE node_type;
	JSON_NODE_SUBTYPE node_subtype;
}raw_field_t;

typedef enum
{
	JSON_DECODE_BEGIN = 0,
	JSON_DECODE_KEY_BEGIN,
	JSON_DECODE_KEY_END,
	JSON_DECODE_FIELD_BEGIN,
	JSON_DECODE_FIELD_END,
	JSON_DECODE_MAX
}JSON_DECODE_STAGE;

// ##########################################################################################################
// ##########################################    PRIVATE HELPER    ##########################################

static bool find_free_storage(tree_node_t* root, tree_node_t** new_elem)
{
	static uint_fast32_t it;
	*new_elem = NULL;
	// printf("Looking for free space (buffer_width=%d)\n", root->buffer_width);
	for(it = 1; it < root->buffer_width; ++it)
	{
		if(root[it].type == JSON_TYPE_EMPTY)
		{
			// printf("Found a spot at ind=%d\n", it);
			*new_elem = &root[it];
			return true;	
		}
	}
	return false;
}

// ##########################################################################################################

static JSON_RETURN_T get_next(char* str_buffer, raw_field_t* result)
{
	static JSON_DECODE_STAGE 	stage;
	static char* 				quotes[2];
	// static int_fast8_t		 	braces_cnt[2];

	/* CHECK
	 * --------------
	 */
	if(str_buffer == NULL)
		return JSON_ERROR_INVALID_BUFFER;

	if(result == NULL)
		return JSON_ERROR_INVALID_BUFFER;

	/* INIT
	 * ---------------
	 * Initialize all variables
	 */
	result->key_begin 	= NULL;
	result->field_begin = NULL;
	result->key_len 	= 0;
	result->field_len 	= 0;
	result->node_type 	= JSON_TYPE_EMPTY;
	result->node_subtype= JSON_SUBTYPE_EMPTY;
	stage 				= JSON_DECODE_BEGIN;
	memset(quotes, 0, 2 * sizeof(char*));
	// braces_cnt[0] = 0;
	// braces_cnt[1] = 0;

	/* ITERATE
	 * ---------------------
	 * Iterate over the string. Stop if NULL-termination is encountered (which should never happen,
	 * if the JSON is valid)
	 */
	while(*str_buffer != '\0')
	{
		switch(stage)
		{
			// Begin
			case JSON_DECODE_BEGIN:
			case JSON_DECODE_KEY_BEGIN: 
			case JSON_DECODE_KEY_END: 
				if(*str_buffer == '\"')
				{
					quotes[0] = quotes[1];
					quotes[1] = str_buffer;
				} 
				else if(*str_buffer == ':')
				{
					if((quotes[0] != NULL) && (quotes[1] != NULL))
					{
						result->key_begin = quotes[0] + 1;
						result->key_len = (uint_fast8_t)(quotes[1] - result->key_begin);
						stage = JSON_DECODE_FIELD_BEGIN;
					}
					else
						return JSON_RETURN_MALFORMED;
				}
				break;
			// determine the length (the strategy depends on the node type)
			case JSON_DECODE_FIELD_BEGIN:
				// STRING 
				if(*str_buffer == '\"')
				{
					/* If a bracket is within a string, this would fuck up the parser. Therefore 
					 * the string field type needs to be recognized, even though it does not need
					 * special treatment afterwards.
					 */
					if(result->node_type == JSON_TYPE_EMPTY)
					{
						result->node_type = JSON_TYPE_KEY;
						result->node_subtype = JSON_SUBTYPE_STRING;
						result->field_begin = str_buffer;
						return JSON_SUCCESS;
					}
				}
				// OBJECT 
				else if(*str_buffer == '{')
				{
					// node is object type
					if(result->node_type == JSON_TYPE_EMPTY)
					{
						result->node_type = JSON_TYPE_OBJ;
						result->field_begin = str_buffer + 1;
						return JSON_SUCCESS;
					}
				}
				// ARRAY 
				else if(*str_buffer == '[')
				{
					if(result->node_type == JSON_TYPE_EMPTY)
					{
						// node is array type
						result->node_type = JSON_TYPE_ARRAY;
						result->field_begin = str_buffer + 1;
						return JSON_SUCCESS;
					}
				}
				// NUMBER 
				else if((*str_buffer >= '0') && (*str_buffer <= '9'))
				{
					if(result->node_type == JSON_TYPE_EMPTY)
					{
						result->node_type = JSON_TYPE_KEY;
						result->field_begin = str_buffer;
						return JSON_SUCCESS;
					}
				}
				// BOOLEAN 
				else if((*str_buffer == 'T') || (*str_buffer == 't'))
				{
					if(result->node_type == JSON_TYPE_EMPTY)
					{
						result->node_type = JSON_TYPE_KEY;
						result->node_subtype = JSON_SUBTYPE_BOOL;
						result->field_begin = str_buffer;
						result->bool_val = true;
						return JSON_SUCCESS;
					}
				}
				else if((*str_buffer == 'F') || (*str_buffer == 'f'))
				{
					if(result->node_type == JSON_TYPE_EMPTY)
					{
						result->node_type = JSON_TYPE_KEY;
						result->node_subtype = JSON_SUBTYPE_BOOL;
						result->field_begin = str_buffer;
						result->bool_val = false;
						return JSON_SUCCESS;
					}
				}
				else if((*str_buffer == ',') || (*str_buffer == '.'))
					return JSON_RETURN_MALFORMED;
				
				break;
		}

		str_buffer++;
	}

	if(stage == JSON_DECODE_BEGIN)
		return JSON_RETURN_NO_MORE_KEYS;
	else
		return JSON_RETURN_MALFORMED;
}

// ##########################################################################################################

static JSON_RETURN_T copy_field(tree_node_t* node, void* val, JSON_NODE_SUBTYPE val_type)
{
	static char* iterator;
	static char convStr[15];

	if(node->raw_decoded)
	{
		switch(val_type)
		{
			case JSON_SUBTYPE_INT: 
			case JSON_SUBTYPE_FLOAT:
				iterator = node->strVal;
				while(	(*iterator != '\0') && 
						(*iterator != ',') && 
						(*iterator != ']') && 
						(*iterator != '}'))
				{
					iterator++;
				}

				strncpy(convStr, node->strVal, (uint_fast8_t)(iterator - node->strVal));

				switch(val_type)
				{
					case JSON_SUBTYPE_INT: *((int32_t*)val) = atoi(convStr); break;
					case JSON_SUBTYPE_FLOAT: *((float*)val) = atof(convStr); break;
				}
				return JSON_SUCCESS;
			case JSON_SUBTYPE_BOOL:
				*((bool*)val) = node->bVal;
				return JSON_SUCCESS;
			case JSON_SUBTYPE_STRING:
				strcpy((char*)val, node->strVal);
				return JSON_SUCCESS;
		}
	}
	else
	{
		switch(val_type)
		{
			case JSON_SUBTYPE_INT: 
				*((int32_t*)val) = *node->iVal;
				return JSON_SUCCESS;
			case JSON_SUBTYPE_FLOAT:
				*((float*)val) = *node->fVal;
				return JSON_SUCCESS;
			case JSON_SUBTYPE_BOOL:
				*((bool*)val) = node->bVal;
				return JSON_SUCCESS;
			case JSON_SUBTYPE_STRING:
				strcpy((char*)val, node->strVal);
				return JSON_SUCCESS;
		}
	}

	return JSON_ERROR_INVALID_ELEMENT_SUBTYPE;
}

// ##########################################################################################################
// ##########################################      PUBLIC API      ##########################################

JSON_RETURN_T json_parse(tree_node_t* root, uint32_t buffer_size, char* str_buffer)
{
	static int_fast32_t 	strLen;
	static char* 			iterator;
	static raw_field_t 		ptr;
	static JSON_RETURN_T 	res;
	static tree_node_t* 	currRoot;
	static tree_node_t* 	newElem;

	/* INPUT CHECK
	 */
	if(buffer_size == 0)	
		return JSON_ERROR_INVALID_BUFFER_SIZE;

	if((root == NULL) || (str_buffer == NULL))
		return JSON_ERROR_INVALID_BUFFER;

	strLen = strlen(str_buffer);

	if(strLen <= 0)
		return JSON_RETURN_MALFORMED;

	/* ROOT NODE INIT
	 */
	memset(root, 0, sizeof(tree_node_t) * buffer_size);

	root[0].type = JSON_TYPE_EMPTY;
	root[0].subtype = JSON_SUBTYPE_EMPTY;
	root[0].buffer_width = buffer_size;
	root[0].visible		= true; 
	strcpy(root[0].key, "root");

	/* FIND BEGINNING
	 */
	iterator = str_buffer;
	while(*iterator != '\0')
	{
		if(*iterator == '{')
		{
			root->type = JSON_TYPE_OBJ;
			root->strVal = iterator;
			break;
		}
		else if(*iterator == '[')
		{
			root->type = JSON_TYPE_ARRAY;
			root->strVal = iterator;
			break;
		}

		iterator++;
	}

	if(root->type == JSON_TYPE_EMPTY)
		return JSON_RETURN_MALFORMED;

	/* START PARSING
	 */
	currRoot = root;
	res = get_next(str_buffer, &ptr);
	while(res == JSON_SUCCESS)
	{
		if(find_free_storage(root, &newElem))
		{
			newElem->parent 		= currRoot;
			newElem->type 			= ptr.node_type;
			newElem->subtype 		= ptr.node_subtype;
			newElem->strVal 		= ptr.field_begin;
			newElem->visible 		= true;
			newElem->raw_decoded 	= true;
			strncpy(newElem->key, ptr.key_begin, ptr.key_len);

			if((newElem->parent->child_count+1) >= MAX_WIDTH_PER_LAYER)
				return JSON_RETURN_TOO_MANY_CHILDREN;

			newElem->parent->child_count++;
			newElem->parent->childs[newElem->parent->child_count] = newElem;


			iterator = newElem->strVal;
			switch(ptr.node_type)
			{
				case JSON_TYPE_KEY:
					switch(ptr.node_subtype)
					{
						case JSON_SUBTYPE_BOOL:
							newElem->bVal = ptr.bool_val;
							break;
						case JSON_SUBTYPE_EMPTY:
							newElem->subtype = JSON_SUBTYPE_INT;
							while(	(*iterator != '\0') && 
									(*iterator != ',') && 
									(*iterator != ']') && 
									(*iterator != '}'))
							{
								if(*iterator == '.')
								{
									newElem->subtype = JSON_SUBTYPE_FLOAT;
									break;
								}
								
								iterator++;
							}
							break;
						case JSON_SUBTYPE_STRING:
							newElem->strVal++;
							iterator = newElem->strVal;
							while(*iterator != '\0') 
							{
								if(*iterator == '\"')
								{
									*iterator = '\0';
									break;
								}
								
								iterator++;
							}
							iterator++;
							break;
					}
					break;
				case JSON_TYPE_OBJ:
					currRoot = newElem;

					break;
				case JSON_TYPE_ARRAY:
					break;
				default:
					return JSON_RETURN_MALFORMED;
			}

			if(1)
			{
				printf("Node: %s\n", newElem->key);
				printf("  Parent: %s\n", newElem->parent->key);
				printf("    Type: %d\n", newElem->type);
				printf("  Subype: %d\n", newElem->subtype);
			}

			res = get_next(iterator, &ptr);
		}
		else
			return JSON_RETURN_NO_MORE_SPACE;
	}

	printf("Parsing finished with ret=%d\n", res);
	return res;	
}

// ##########################################################################################################

JSON_RETURN_T json_geti(tree_node_t* node, int32_t* val)
{ return copy_field(node, (void*) val, JSON_SUBTYPE_INT); }

// ##########################################################################################################

JSON_RETURN_T json_getf(tree_node_t* node, float* val)
{ return copy_field(node, (void*) val, JSON_SUBTYPE_FLOAT); }

// ##########################################################################################################

JSON_RETURN_T json_getstr(tree_node_t* node, char* val)
{ return copy_field(node, (void*) val, JSON_SUBTYPE_STRING); }

// ##########################################################################################################

JSON_RETURN_T json_getb(tree_node_t* node, bool* val)
{ return copy_field(node, (void*) val, JSON_SUBTYPE_BOOL); }

// ##########################################################################################################


#ifdef __cplusplus
}
#endif


#endif