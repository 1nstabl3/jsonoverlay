#ifndef JSON_DECODE_H
#define JSON_DECODE_H

#include "jsoncommon.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Parses a JSON string into a tree representation
 * @details [long description]
 * 
 * @note Not yet implemented
 * 
 * @param root [description]
 * @param buffer_size [description]
 * @param str_buffer [description]
 * @return [description]
 */
JSON_RETURN_T json_parse(tree_node_t* root, uint32_t buffer_size, char* str_buffer);

JSON_RETURN_T json_geti(tree_node_t* root, int32_t* val);
JSON_RETURN_T json_getf(tree_node_t* root, float* val);
JSON_RETURN_T json_getstr(tree_node_t* root, char* val);
JSON_RETURN_T json_getb(tree_node_t* root, bool* val);


#ifdef __cplusplus
}
#endif


#endif