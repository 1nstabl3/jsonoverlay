#ifndef JSON_OVERLAY
#define JSON_OVERLAY

#include "jsonoverlay.h"

#include <string.h>
#include <assert.h>
#include <stdio.h>


#ifdef __cplusplus
extern "C" {
#endif

// ##########################################################################################################
// ##########################################    PRIVATE HELPER    ##########################################

static bool find_free_storage(tree_node_t* root, tree_node_t** new_elem)
{
	static uint_fast32_t it;
	*new_elem = NULL;
	// printf("Looking for free space (buffer_width=%d)\n", root->buffer_width);
	for(it = 1; it < root->buffer_width; ++it)
	{
		if(root[it].type == JSON_TYPE_EMPTY)
		{
			// printf("Found a spot at ind=%d\n", it);
			*new_elem = &root[it];
			return true;	
		}
	}
	return false;
}

// ##########################################################################################################

static void clear_self_ref(tree_node_t* node)
{
	static uint_fast32_t it;
	for(it = 0; it < node->parent->child_count; ++it)
		if(node->parent->childs[it] == node)
		{
			// printf("Found self-reference\n");
			node->parent->childs[it] = NULL;
			node->parent->child_count--;
			break;	
		}	
}

// ##########################################################################################################

static inline void add_literal(char* str_buffer, uint_fast32_t* strLen, char* literal)
{
	strcat(str_buffer, literal);
	(*strLen)++;
}

// ##########################################################################################################

static int32_t append_key_field(char* buffer, char* key)
{
	assert(buffer != NULL);
	assert(key != NULL);

	strcat(buffer, "\"");
	strcat(buffer, key);
	strcat(buffer, "\":");

	return 3 + strlen(key);
}

// ##########################################################################################################

static JSON_RETURN_T add_node(tree_node_t* root, tree_node_t* parent, tree_node_t** self, char* key, JSON_NODE_TYPE type, JSON_NODE_SUBTYPE subtype , void* value)
{
	static tree_node_t* elem;

	assert(root != NULL);
	assert(parent != NULL);
	assert(key != NULL);

	// Check whether the parent can accept more children
	if((parent->child_count+1) >= MAX_WIDTH_PER_LAYER)
		return JSON_RETURN_TOO_MANY_CHILDREN;

	// Find free elemnt for storage
	elem = NULL;
	if(!find_free_storage(root, &elem))
		return JSON_RETURN_NO_MORE_SPACE;
		

	// create element itself
	assert(elem != NULL);
	// printf("Assigning values to new element\n");
	elem->type 			= type; 
	elem->subtype 		= subtype;
	elem->child_count 	= 0; 
	elem->parent		= parent; 
	elem->visible		= true; 
	strncpy(elem->key, key, MAX_KEY_LENGTH);
	elem->buffer_width 	= root[0].buffer_width;

	if(type == JSON_TYPE_KEY)
	{
		switch(subtype)
		{
			case JSON_SUBTYPE_INT:
				elem->iVal 	= (int32_t*)value;
				break;
			case JSON_SUBTYPE_FLOAT:
				elem->fVal 	= (float*)value;
				break;
			case JSON_SUBTYPE_STRING:
				elem->strVal = (char*)value;
				break;
			default:
				return JSON_ERROR_INVALID_ELEMENT_SUBTYPE;
		}
	}



	// assign new element to parent
	// printf("Injecting new element to parent\n");
	parent->childs[parent->child_count] = elem;

	// increase child count of parent 
	parent->child_count++;

	if(self != NULL)
		*self = elem;

	return JSON_SUCCESS;
}

// ##########################################################################################################

static JSON_RETURN_T json_add_array(tree_node_t* root, tree_node_t* parent, char* key, JSON_NODE_SUBTYPE subtype, void* value, uint32_t val_len)
{
	static tree_node_t* elem;

	assert(parent != NULL);
	assert(root != NULL);
	assert(key != NULL);
	assert(value != NULL);

	// printf("Adding array %s of %d elements\n", key, val_len);
	// Find free elemnt for storage
	elem = NULL;
	if(!find_free_storage(root, &elem))
		return JSON_RETURN_NO_MORE_SPACE;
		
	// Check whether the parent can accept more children
	if((parent->child_count+1) >= MAX_WIDTH_PER_LAYER)
		return JSON_RETURN_TOO_MANY_CHILDREN;

	// create element itself
	assert(elem != NULL);
	// printf("Assigning values to new element\n");
	elem->type 			= JSON_TYPE_ARRAY; 
	elem->subtype 		= subtype; 
	elem->child_count 	= val_len;

	switch(subtype)
	{
		case JSON_SUBTYPE_INT:
			elem->iVal 	= (int32_t*)value;
			break;
		case JSON_SUBTYPE_FLOAT:
			elem->fVal 	= (float*)value;
			break;
		case JSON_SUBTYPE_STRING:
			elem->strVal = (char*)value;
			break;
		default:
			return JSON_ERROR_INVALID_ELEMENT_SUBTYPE;
	}

	elem->parent		= parent; 
	elem->visible		= true; 
	strncpy(elem->key, key, MAX_KEY_LENGTH);
	elem->buffer_width 	= root[0].buffer_width;

	// assign new element to parent
	// printf("Injecting new element to parent\n");
	parent->childs[parent->child_count] = elem;

	// increase child count of parent 
	parent->child_count++;

	return JSON_SUCCESS;
}

// ##########################################################################################################
// ##########################################      PUBLIC API      ##########################################

JSON_RETURN_T json_add_key_str(tree_node_t* root, tree_node_t* parent, char* key, char* value)
{
	return add_node(root, parent, NULL, key, JSON_TYPE_KEY, JSON_SUBTYPE_STRING, (void*)value);
}

// ##########################################################################################################

JSON_RETURN_T json_add_key_i32(tree_node_t* root, tree_node_t* parent, char* key, int32_t* value)
{
	return add_node(root, parent, NULL, key, JSON_TYPE_KEY, JSON_SUBTYPE_INT, (void*)value);
}

// ##########################################################################################################

JSON_RETURN_T json_add_key_f(tree_node_t* root, tree_node_t* parent, char* key, float* value)
{
	return add_node(root, parent, NULL, key, JSON_TYPE_KEY, JSON_SUBTYPE_FLOAT, (void*)value);
}

// ##########################################################################################################

JSON_RETURN_T json_add_obj(tree_node_t* root, tree_node_t* parent, tree_node_t** self, char* key)
{
	return add_node(root, parent, self, key, JSON_TYPE_OBJ, JSON_SUBTYPE_EMPTY, NULL);
}

// ##########################################################################################################

JSON_RETURN_T json_add_array_i32(tree_node_t* root, tree_node_t* parent, char* key, int32_t* val, uint32_t val_len)
{
	return json_add_array(root, parent, key, JSON_SUBTYPE_INT, (void*)val, val_len);
}

// ##########################################################################################################

JSON_RETURN_T json_add_array_f(tree_node_t* root, tree_node_t* parent, char* key, float* val, uint32_t val_len)
{
	return json_add_array(root, parent, key, JSON_SUBTYPE_FLOAT, (void*)val, val_len);
}

// ##########################################################################################################

JSON_RETURN_T json_add_node(tree_node_t* dest, tree_node_t* node)
{
	assert(dest != NULL);
	assert(node != NULL);

	if((node == NULL) || (dest == NULL))
		return JSON_ERROR_INVALID_BUFFER;

	if(dest->child_count >= MAX_WIDTH_PER_LAYER)
		return JSON_RETURN_TOO_MANY_CHILDREN;

	dest->childs[dest->child_count++] = node;
	node->parent = dest;

	return JSON_SUCCESS;
}

// ##########################################################################################################

JSON_RETURN_T json_remove_node(tree_node_t* node)
{
	int_fast32_t it;
	static JSON_RETURN_T err;

	assert(node != NULL);

	if(node == NULL)
		return JSON_ERROR_INVALID_BUFFER;

	// printf("Removing node '%s'\n", node->key);

	if((node->child_count != 0) && (node->type != JSON_TYPE_ARRAY))
	{
		// printf("Traversing children\n");
		for(it = 0; it < node->child_count; ++it)
		{
			err = json_remove_node(node->childs[it]);
			if(err != JSON_SUCCESS)
				return err;
			node->child_count--;
		}
	}
	
	// printf("Clearing self\n");
	node->type = JSON_TYPE_EMPTY;
	node->child_count = 0;
	// printf("Removing self-reference of parent element ('%s')\n", node->parent->key);
	clear_self_ref(node);

	// printf("Done\n");
	return JSON_SUCCESS;
}

// ##########################################################################################################

JSON_RETURN_T json_move_node(tree_node_t* dest, tree_node_t* node)
{
	static uint_fast32_t it;
	assert(dest != NULL);
	assert(node != NULL);

	if((node == NULL) || (dest == NULL))
		return JSON_ERROR_INVALID_BUFFER;

	if(dest->child_count >= MAX_WIDTH_PER_LAYER)
		return JSON_RETURN_TOO_MANY_CHILDREN;

	for(it = 0; it < MAX_WIDTH_PER_LAYER; ++it)
		if(dest->childs[it] == NULL)
			break;	
	dest->childs[it] = node;
	dest->child_count++;
	clear_self_ref(node);
	node->parent = dest;

	return JSON_SUCCESS;
}


// ##########################################################################################################

JSON_RETURN_T json_build(tree_node_t* root, char* str_buffer, uint32_t buffer_size, bool pretty)
{
	static uint_fast32_t strLen;
	static char numBuffer[30];
	static char strIndent[40];
	static bool indended = false;
	static bool is_root_call = true;
	static JSON_RETURN_T err;
	int_fast32_t it;
	int_fast32_t ind;
	const bool is_array = (root->type == JSON_TYPE_ARRAY);
	bool local_is_root_call = is_root_call;

	assert(root != NULL);
	assert(str_buffer != NULL);

	if(is_root_call == true)
		is_root_call = false;

	// Clear buffer
	memset(str_buffer, 0, buffer_size * sizeof(char));
	strLen = 0;

	if(root->type == JSON_TYPE_OBJ)
		add_literal(str_buffer, &strLen, "{");
	else if(root->type == JSON_TYPE_ARRAY)
		add_literal(str_buffer, &strLen, "[");
	else
		return JSON_RETURN_MALFORMED;


	if(pretty)
	{

		if(root->child_count > 3)
		{
			strcat(strIndent, "    ");
			indended = true;
			add_literal(str_buffer, &strLen, "\n");
		}
		else
			indended = false;
	}

	// printf("%s: Child Count = %d\n", root->key, root->child_count);
	ind = 0;
	for(it = 0; it < root->child_count; ++it)
	{
		if((ind >= MAX_WIDTH_PER_LAYER) && (!is_array))
			break;

		if(!is_array)
		{
			if(root->childs[ind] == NULL)
			{
				it--;
				ind++;
				continue;
			}

			// printf("key=%s\tvisible=%d\n", root->childs[ind]->key, root->childs[ind]->visible);
			if(root->childs[ind]->visible == false)
			{
				ind++;
				continue;
			}
		}


		strcat(str_buffer, strIndent);
		strLen += strlen(strIndent);

		if(!is_array)
		{
			strLen += append_key_field(str_buffer, root->childs[ind]->key);
		}

		if((!is_array) && (root->childs[ind]->type == JSON_TYPE_KEY))
		{

			if(root->childs[ind]->subtype == JSON_SUBTYPE_STRING)
			{
				add_literal(str_buffer, &strLen, "\"");
				strcat(str_buffer, root->childs[ind]->strVal);
				strLen += strlen(root->childs[ind]->strVal);
				add_literal(str_buffer, &strLen, "\"");
			}
			else if(root->childs[ind]->subtype == JSON_SUBTYPE_INT)
			{
				sprintf(numBuffer, "%d" , *root->childs[ind]->iVal);
				strcat(str_buffer, numBuffer);
				strLen += strlen(numBuffer);
			}
			else if(root->childs[ind]->subtype == JSON_SUBTYPE_FLOAT)
			{
				sprintf(numBuffer, "%f" , *root->childs[ind]->fVal);
				strcat(str_buffer, numBuffer);
				strLen += strlen(numBuffer);
			}
			else if(root->childs[ind]->subtype == JSON_SUBTYPE_BOOL)
			{
				if(root->childs[ind]->bVal == true)
				{
					strcat(str_buffer, "True");
					strLen += 4;
				}
				else
				{
					strcat(str_buffer, "False");
					strLen += 5;
				}
			}
			else
				printf("[ ! ] Unknown Subtype encountered.\nDropping it\n");

		}
		else if (is_array)
		{
			if(root->subtype == JSON_SUBTYPE_STRING)
			{
				add_literal(str_buffer, &strLen, "\"");
				// strcat(str_buffer, root->strVal[it]);
				// strLen += strlen(root->childs[ind]->strVal);
				add_literal(str_buffer, &strLen, "\"");
			}
			else if(root->subtype == JSON_SUBTYPE_INT)
			{
				sprintf(numBuffer, "%d" , root->iVal[it]);
				strcat(str_buffer, numBuffer);
				strLen += strlen(numBuffer);
			}
			else if(root->subtype == JSON_SUBTYPE_FLOAT)
			{
				sprintf(numBuffer, "%f" , root->fVal[it]);
				strcat(str_buffer, numBuffer);
				strLen += strlen(numBuffer);
			}
			else if(root->subtype == JSON_SUBTYPE_BOOL)
			{
				if(root->iVal[it] > 0)
				{
					strcat(str_buffer, "True");
					strLen += 4;
				}
				else
				{
					strcat(str_buffer, "False");
					strLen += 5;
				}
			}
			else	
				printf("[ ! ] Unknown Subtype encountered.\nDropping it\n");
		}
		else if(root->childs[ind]->type == JSON_TYPE_OBJ)
		{
			if((err = json_build(root->childs[ind], str_buffer + strLen, buffer_size - strLen-1, pretty)) != JSON_SUCCESS)
			{
				return err; 
			}
			strLen = strlen(str_buffer);
		}
		else if(root->childs[ind]->type == JSON_TYPE_ARRAY)
		// else if(is_array)
		{
			
			{
				if((err = json_build(root->childs[ind], str_buffer + strLen, buffer_size - strLen-1, pretty)) != JSON_SUCCESS)
				{
					return err; 
				}
				strLen = strlen(str_buffer);
			}	
		}

		if(ind < (root->child_count - 1))
		{
			add_literal(str_buffer, &strLen, ",");
		}

		if((pretty) && (root->child_count > 3))
		{
			add_literal(str_buffer, &strLen, "\n");
		}

		ind++;
	}


	if(pretty)
	{
		if(indended)
			strIndent[strlen(strIndent) - 4] = '\0';
	}

	if(local_is_root_call)
	{
		is_root_call = true;
		strIndent[0] = '\0';
	}

	if(!is_root_call)
	{
		strcat(str_buffer, strIndent);
		strLen += strlen(strIndent);
	}

	if(root->type == JSON_TYPE_OBJ)
	{
		add_literal(str_buffer, &strLen, "}");
	}
	else if(root->type == JSON_TYPE_ARRAY)
	{
		add_literal(str_buffer, &strLen, "]");
	}
	else
		return JSON_RETURN_MALFORMED;


	return JSON_SUCCESS;
}

#ifdef __cplusplus
}
#endif


#endif