#ifndef JSON_COMMON_H
#define JSON_COMMON_H

#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

#define MAX_WIDTH_PER_LAYER 10	
#define MAX_KEY_LENGTH 		30
#define MAX_FIELD_LENGTH 	130

typedef enum 
{
	JSON_TYPE_EMPTY= 0,
	JSON_TYPE_OBJ,
	JSON_TYPE_ARRAY,
	JSON_TYPE_KEY,
	JSON_NODE_TYPE_MAX	
} JSON_NODE_TYPE;

typedef enum 
{
	JSON_SUBTYPE_EMPTY = 0,
	JSON_SUBTYPE_STRING,
	JSON_SUBTYPE_INT,
	JSON_SUBTYPE_FLOAT,
	JSON_SUBTYPE_BOOL,
	JSON_NODE_SUBTYPE_MAX	
} JSON_NODE_SUBTYPE;

typedef enum 
{
	JSON_SUCCESS = 0,
	JSON_ERROR_INVALID_BUFFER,	
	JSON_ERROR_INVALID_BUFFER_SIZE,	
	JSON_ERROR_INVALID_ROOT_ELEMENT_TYPE,	
	JSON_ERROR_INVALID_ELEMENT_SUBTYPE,	
	JSON_RETURN_NO_MORE_SPACE,
	JSON_RETURN_TOO_MANY_CHILDREN,
	JSON_RETURN_MALFORMED,
	JSON_RETURN_NO_MORE_KEYS,
	JSON_ERROR_MAX	
} JSON_RETURN_T;

typedef struct tree_node
{
	char 				key[MAX_KEY_LENGTH + 1];
	union 
	{
		const char* 	strVal;
		const int32_t* 	iVal;
		const float*	fVal; 
		bool			bVal; 
	};

	bool 				raw_decoded;
	JSON_NODE_TYPE 		type;
	JSON_NODE_SUBTYPE 	subtype;
	
	struct tree_node* 	parent;
	struct tree_node* 	childs[MAX_WIDTH_PER_LAYER];
	uint32_t 			child_count;
	uint32_t 			buffer_width;
	bool 				visible;
}tree_node_t;

/**
 * @brief Initializes the document representation
 * @details [long description]
 * 
 * @param root Pointer to the first element of the document buffer
 * @param root_type Defines whether the document is an object or array
 * @param buffer_width Number of elements in the document buffer (aka maximum possible nodes in a document)
 * @return Returns JSON_SUCCESS on success
 */
JSON_RETURN_T json_init(tree_node_t* root, JSON_NODE_TYPE root_type, uint32_t buffer_width);

/**
 * @brief Hides node and its childs from JSON output generation
 * @details [long description]
 * 
 * @param node [description]
 * @return [description]
 */
JSON_RETURN_T json_hide_node(tree_node_t* node);

/**
 * @brief Includes node and its childs to JSON output generation
 * @details [long description]
 * 
 * @param node [description]
 * @return [description]
 */
JSON_RETURN_T json_show_node(tree_node_t* node);

/**
 * @brief Returns the reference to the first node, that matches the given key
 * @details [long description]
 * 
 * @param root [description]
 * @param key [description]
 * 
 * @return [description]
 */
tree_node_t* json_get_node(tree_node_t* root, char* key);


JSON_NODE_TYPE json_get_type(tree_node_t* node);
JSON_NODE_SUBTYPE json_get_subtype(tree_node_t* node);
JSON_NODE_TYPE json_get_type_by_key(tree_node_t* root, char* key);
JSON_NODE_SUBTYPE json_get_subtype_by_key(tree_node_t* root, char* key);

#ifdef __cplusplus
}
#endif


#endif