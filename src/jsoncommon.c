#ifndef JSON_COMMON
#define JSON_COMMON

#include "jsoncommon.h"

#include <string.h>
#include <assert.h>
#include <stdio.h>


#ifdef __cplusplus
extern "C" {
#endif

// ##########################################################################################################
// ##########################################    PRIVATE HELPER    ##########################################

static bool is_equal(char* str1, char* str2)
{
	static bool is_equal_length;
	static bool contains_str2;

	is_equal_length = (strlen(str1) == strlen(str2));
	contains_str2 = (strstr(str1, str2) != NULL);

	return (is_equal_length && contains_str2);
}

// ##########################################################################################################

static JSON_RETURN_T json_node_visible(tree_node_t* node, bool show)
{
	assert(node != NULL);

	if(node == NULL)
		return JSON_ERROR_INVALID_BUFFER;

	node->visible = show;
	return JSON_SUCCESS;
}

// ##########################################################################################################
// ##########################################      PUBLIC API      ##########################################

JSON_RETURN_T json_init(tree_node_t* root, JSON_NODE_TYPE root_type, uint32_t buffer_width)
{
	assert(root != NULL);
	assert(buffer_width > 0);

	if(root == NULL)
		return JSON_ERROR_INVALID_BUFFER;

	if(buffer_width <= 0)
		return JSON_ERROR_INVALID_BUFFER_SIZE;

	if((root_type != JSON_TYPE_OBJ) && (root_type != JSON_TYPE_ARRAY))
		return JSON_ERROR_INVALID_ROOT_ELEMENT_TYPE;

	memset(root, 0, sizeof(tree_node_t) * buffer_width);

	root[0].type = root_type;
	root[0].buffer_width = buffer_width;
	root[0].visible		= true; 
	strcpy(root[0].key, "root");

	return JSON_SUCCESS;
}

// ##########################################################################################################

tree_node_t* json_get_node(tree_node_t* root, char* key)
{
	assert(root != NULL);
	assert(key != NULL);

	static tree_node_t* result = NULL;
	static tree_node_t* candidate = NULL;
	int_fast32_t it;

	// check if root is already the key
	if(is_equal(root->key,key))
		return root;

	// check if last result is searched for again
	// This might give a unexpected result, if the same key exists in another branch than root
	if(result != NULL)
	{
		if(is_equal(result->key,key))
		{
			candidate = result;
			/* traverse back to the tree root and check if the search root is on that way
			 * effectivly meaning, result is a child of root and therefore the searched element
			 */
			while(candidate->parent != NULL)	
			{
				if(candidate->parent == root)
					return result;
				candidate = candidate->parent;
			}
		}
	}

	/* @TODO For arrays of objects there needs to be a 2nd decision path
	*/
	if(root->type != JSON_TYPE_ARRAY)
	{
		// check all children
		// printf("Searching in all children of %s\n", root->key);
		for(it = 0; it < root->child_count; ++it)
		{
			if(is_equal(root->childs[it]->key,key) )
				return root->childs[it];
		}

		// traverse down into tree until all leaves have been visited
		// printf("Searching in all childrens children\n");
		for(it = 0; it < root->child_count; ++it)
		{
			result = json_get_node(root->childs[it], key);
			if(result != NULL)
				return result;
		}
	}

	return NULL;
}

// ##########################################################################################################

JSON_RETURN_T json_hide_node(tree_node_t* node)
{
	return json_node_visible(node, false);
}

// ##########################################################################################################

JSON_RETURN_T json_show_node(tree_node_t* node)
{
	return json_node_visible(node, true);
}

// ##########################################################################################################

JSON_NODE_TYPE json_get_type(tree_node_t* node)
{
	if(node == NULL)
		return JSON_TYPE_EMPTY;
	else
		return node->type;
}

// ##########################################################################################################

JSON_NODE_SUBTYPE json_get_subtype(tree_node_t* node)
{
	if(node == NULL)
		return JSON_SUBTYPE_EMPTY;
	else
		return node->subtype;
}

// ##########################################################################################################

JSON_NODE_TYPE json_get_type_by_key(tree_node_t* root, char* key)
{
	return json_get_type(json_get_node(root, key));
}

// ##########################################################################################################

JSON_NODE_SUBTYPE json_get_subtype_by_key(tree_node_t* root, char* key)
{
	return json_get_subtype(json_get_node(root, key));
}

#ifdef __cplusplus
}
#endif


#endif