
#include "jsonoverlay.h"

#include <stdio.h>

static tree_node_t doc[100];
static char strJSON[1000];

int main()
{
	JSON_RETURN_T err;
	if((err = json_init(doc, JSON_TYPE_OBJ, 100)) != JSON_SUCCESS)
	{
		printf("Initialization failed with code %d\n", err);
		return -1;
	}

	tree_node_t* root = doc;

	static char key1_val[] = "Hello World";
	static char key2_val[] = "I am";
	static char key3_val[] = "mbedJSON";
	static int32_t key4_val = -1234567;
	static float key5_val = -3.14;
	static int32_t key6_val[5] = {-1234567, 1, 0, 12, 43};
	static float key7_val[5] = {-1234567.0f, 1.3f, 0.1f, 0.00043f, 43};
	
	if((err= json_add_key_str(doc, root, "keyStr1", key1_val))  != JSON_SUCCESS)
	{
		printf("Adding string key failed with code %d\n", err);
		return -1;
	}
	
	if((err = json_add_key_str(doc, root, "keyStr2", key2_val))  != JSON_SUCCESS)
	{
		printf("Adding string key failed with code %d\n", err);
		return -1;
	}
	
	if((err = json_add_key_str(doc, root, "keyStr3", key3_val))  != JSON_SUCCESS)
	{
		printf("Adding string key failed with code %d\n", err);
		return -1;
	}

	if((err = json_add_key_i32(doc, root, "keyi", &key4_val))  != JSON_SUCCESS)
	{
		printf("Adding int key failed with code %d\n", err);
		return -1;
	}

	if((err = json_add_key_f(doc, root, "keyf", &key5_val))  != JSON_SUCCESS)
	{
		printf("Adding float key failed with code %d\n", err);
		return -1;
	}


	tree_node_t* self;
	if((err = json_add_obj(doc, doc, &self, "obj1"))  != JSON_SUCCESS)
	{
		printf("Adding object failed with code %d\n", err);
		return -1;
	}

	if((err = json_add_obj(doc, self, NULL, "obj2"))  != JSON_SUCCESS)
	{
		printf("Adding subobject failed with code %d\n", err);
		return -1;
	}

	if((err = json_add_key_f(doc, self, "keyf", &key5_val))  != JSON_SUCCESS)
	{
		printf("Adding float key failed with code %d\n", err);
		return -1;
	}

	if((err = json_add_key_i32(doc, self, "keyi", &key4_val))  != JSON_SUCCESS)
	{
		printf("Adding int key failed with code %d\n", err);
		return -1;
	}

	if((err = json_add_obj(doc, doc, &self, "obj3"))  != JSON_SUCCESS)
	{
		printf("Adding object failed with code %d\n", err);
		return -1;
	}

	if((err = json_add_key_i32(doc, self, "keyi", &key4_val))  != JSON_SUCCESS)
	{
		printf("Adding int key failed with code %d\n", err);
		return -1;
	}

	if((err= json_add_key_str(doc, self, "keyStr1", key1_val))  != JSON_SUCCESS)
	{
		printf("Adding string key failed with code %d\n", err);
		return -1;
	}

	if((err= json_add_key_str(doc, self, "keyStr1", key2_val))  != JSON_SUCCESS)
	{
		printf("Adding string key failed with code %d\n", err);
		return -1;
	}

	if((err = json_add_key_i32(doc, self, "keyi", &key4_val))  != JSON_SUCCESS)
	{
		printf("Adding int key failed with code %d\n", err);
		return -1;
	}

	if((err = json_add_array_i32(doc, self, "arr1", key6_val, 5))  != JSON_SUCCESS)
	{
		printf("Adding int array failed with code %d\n", err);
		return -1;
	}

	if((err = json_add_array_f(doc, self, "arr2", key7_val, 5))  != JSON_SUCCESS)
	{
		printf("Adding float array failed with code %d\n", err);
		return -1;
	}


	printf("Building JSON string\n");
	err = json_build(doc, strJSON, 1000, true);
	if(err  != JSON_SUCCESS)
	{
		printf("Building JSON string failed with code %d\n", err);
		return -1;
	}
	else
		printf("%s\n", strJSON);


	tree_node_t* obj = json_get_node(doc, "obj3");
	if(obj == NULL)
		printf("[ERR] Element 'obj3' was not found in tree\n");
	else
		printf("Found 'obj3' at 0x%x\n", obj);

	tree_node_t* obj2 = json_get_node(doc, "obj20");
	if(obj2 == NULL)
		printf("[ERR] Element 'obj20' was not found in tree\n");
	else
		printf("Found 'obj20' at 0x%x\n", obj2);



	obj2 = json_get_node(doc, "arr1");
	printf("Excluding (hiding) 'arr1' from output\n");
	if((err = json_hide_node(obj2)) != JSON_SUCCESS)
	{
		printf("[ERR] Failed to hide object with error code %d\n", err);
		return -1;
	}

	printf("Building JSON string\n");
	if((err = json_build(doc, strJSON, 1000, true)) != JSON_SUCCESS)
	{
		printf("[ERR] Failed to build JSON string with error code %d\n", err);
		return -1;
	}
	printf("%s\n", strJSON);

	printf("Reincluding (showing) 'arr1' from output\n");
	if((err = json_show_node(obj2)) != JSON_SUCCESS)
	{
		printf("[ERR] Failed to show object with error code %d\n", err);
		return -1;
	}

	printf("Building JSON string\n");
	if((err = json_build(doc, strJSON, 1000, true)) != JSON_SUCCESS)
	{
		printf("[ERR] Failed to build JSON string with error code %d\n", err);
		return -1;
	}
	printf("%s\n", strJSON);


	printf("Moving 'arr1' to 'obj1'\n");
	obj = json_get_node(doc, "obj1");
	if((err = json_move_node(obj, obj2)) != JSON_SUCCESS)
	{
		printf("[ERR] Failed to move object with error code %d\n", err);
		return -1;
	}

	printf("Building JSON string\n");
	if((err = json_build(doc, strJSON, 1000, true)) != JSON_SUCCESS)
	{
		printf("[ERR] Failed to build JSON string with error code %d\n", err);
		return -1;
	}
	printf("%s\n", strJSON);




	obj = json_get_node(doc, "obj3");
	printf("Removing 'obj3' from tree\n");
	if((err = json_remove_node(obj)) != JSON_SUCCESS)
		printf("[ERR] Failed to remove object with error code %d\n", err);
	else
	{
		printf("Building JSON string\n");
		err = json_build(doc, strJSON, 1000, true);
		if(err  != JSON_SUCCESS)
		{
			printf("Building JSON string failed with code %d\n", err);
			return -1;
		}
		else
			printf("%s\n", strJSON);
	}



	return 0;
}