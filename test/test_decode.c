
#include "jolly.h"

#include <stdio.h>

static tree_node_t doc[100];
static char strJSON[1000];

int main()
{
	JSON_RETURN_T err;

	char testJSON[] = "{\
							\"nodei\" : 123, \
							\"nodef\" : 123.456,\
							\"nodestr\" : \"Hello World\",\
							\"nodeb\" : false,\
							\"nodeobj\" : \
							{\
								\"nodeobji\" : 123, \
								\"nodeobjf\" : 123.456, \
								\"nodeobjstr\" : \"Hello { World }\"\
								\"nodeobjb\" : True\
							} \
						}";

	json_parse(doc, 100, testJSON);

	printf("Building JSON string\n");
		err = json_build(doc, strJSON, 1000, true);
		if(err  != JSON_SUCCESS)
		{
			printf("Building JSON string failed with code %d\n", err);
			return -1;
		}
		else
			printf("%s\n", strJSON);

	printf("Raw string to value\n");
	float valf;
	json_getf(json_get_node(doc, "nodef"), &valf);
	printf("nodef = %f\n", valf);

	return 0;
}