
#include "jsonoverlay.h"

#include <stdio.h>
#include <time.h>
#include <string.h>
#include <math.h>


#define DOC_MAX_NODES 99999
#define DOC_OBJ_RATIO 0.33f

#define STR_BUFFER_SIZE 4000

static tree_node_t doc[DOC_MAX_NODES];
static char strJSON[STR_BUFFER_SIZE];

static volatile clock_t start, diff;

static inline void timing_start()
{
	start = clock();
}

static inline void timing_stop()
{
	diff = clock() - start;
	static double msec;
	msec = (double)diff * 1000.0 / (double)CLOCKS_PER_SEC;
	printf("Time taken %f milliseconds (%ld clock cycles)\n", msec, diff);
}

int main()
{
	static int32_t it;

	tree_node_t* root = doc;
	static char key1_val[] = "Hello World";
	static char key2_val[] = "I am";
	static char key3_val[] = "mbedJSON";
	static int32_t key4_val = -1234567;
	static float key5_val = -3.14;
	static int32_t key6_val[5] = {-1234567, 1, 0, 12, 43};
	static float key7_val[5] = {-1234567.0f, 1.3f, 0.1f, 0.00043f, 43};
	

	// Benchmarking "add" operations on empty documents
	printf("Running add_key_str() %d times\n", DOC_MAX_NODES - 1);
	json_init(doc, JSON_TYPE_OBJ, DOC_MAX_NODES);
	timing_start();
	for(it = 0; it < DOC_MAX_NODES - 1; ++it)
		json_add_key_str(doc, root, "keyStr1", key1_val);
	timing_stop();

	printf("Running add_key_i32() %d times\n", DOC_MAX_NODES - 1);
	json_init(doc, JSON_TYPE_OBJ, DOC_MAX_NODES);
	timing_start();
	for(it = 0; it < DOC_MAX_NODES - 1; ++it)
		json_add_key_i32(doc, root, "keyi", &key4_val);
	timing_stop();

	printf("Running add_key_f() %d times\n", DOC_MAX_NODES - 1);
	json_init(doc, JSON_TYPE_OBJ, DOC_MAX_NODES);
	timing_start();
	for(it = 0; it < DOC_MAX_NODES - 1; ++it)
		json_add_key_f(doc, root, "keyf", &key5_val);
	timing_stop();

	tree_node_t* self;
	printf("Running add_obj() %d times\n", DOC_MAX_NODES - 1);
	json_init(doc, JSON_TYPE_OBJ, DOC_MAX_NODES);
	timing_start();
	for(it = 0; it < DOC_MAX_NODES - 1; ++it)
		json_add_obj(doc, root, &self, "obj1");
	timing_stop();

	printf("Running add_array_i32() %d times\n", DOC_MAX_NODES - 1);
	json_init(doc, JSON_TYPE_OBJ, DOC_MAX_NODES);
	timing_start();
	for(it = 0; it < DOC_MAX_NODES - 1; ++it)
		json_add_array_i32(doc, root, "arr1", key6_val, 5);
	timing_stop();

	printf("Running add_array_f() %d times\n", DOC_MAX_NODES - 1);
	json_init(doc, JSON_TYPE_OBJ, DOC_MAX_NODES);
	timing_start();
	for(it = 0; it < DOC_MAX_NODES - 1; ++it)
		json_add_array_f(doc, root, "arr2", key7_val, 5);
	timing_stop();

	// Building a complex tree for search, remove and build operations
	printf("Constructing tree for coming tests\n");
	json_init(doc, JSON_TYPE_OBJ, DOC_MAX_NODES);
	char node_name[MAX_KEY_LENGTH];
	const int max_obj_per_layer = round(MAX_WIDTH_PER_LAYER * DOC_OBJ_RATIO);
	// const int max_key_per_layer = MAX_WIDTH_PER_LAYER - max_obj_per_layer;
	int depth_lvl = 0, width_lvl;
	
	for(width_lvl = 0; width_lvl < max_obj_per_layer; ++width_lvl)
	{
		sprintf(node_name, "%0x%0x_", depth_lvl, width_lvl);
		json_add_obj(doc, doc, &self, strcat(node_name, "obj"));
	}
	for(width_lvl = max_obj_per_layer + 1; width_lvl < MAX_WIDTH_PER_LAYER; ++width_lvl)
	{
		sprintf(node_name, "%0x%0x_", depth_lvl, width_lvl);
		json_add_key_str(doc, doc, strcat(node_name, "str"), key1_val);
	}

	json_build(doc, strJSON, STR_BUFFER_SIZE, true);
	printf("%s\n", strJSON);
	

	// benchmarking "search" operations
	// printf("Running add_array_f() %d times\n", DOC_MAX_NODES - 1);
	// json_init(doc, JSON_TYPE_OBJ, DOC_MAX_NODES);
	// timing_start();
	// for(it = 0; it < DOC_MAX_NODES - 1; ++it)
	// 	json_add_array_f(doc, root, "arr2", key7_val, 5);
	// timing_stop();

	// benchmarking "build" operation
	printf("Running build() %d times\n", DOC_MAX_NODES - 1);
	timing_start();
	for(it = 0; it < DOC_MAX_NODES - 1; ++it)
		json_build(doc, strJSON, STR_BUFFER_SIZE, true);
	timing_stop();
	printf("%s\n", strJSON);
	// benchmarking "move" operation
	// benchmarking "remove" operation

	return 0;
}